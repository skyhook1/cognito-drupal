import * as dynamoDbLib from "./libs/dynamodb-lib";
var drupalHash = require("drupal-hash");

module.exports.authenticateUser = async (email, password) => {
  // Get the user from the old database
  var user = await getUser(email);
  if (!user) return null;

  // Check password
  var isValid = drupalHash.checkPassword(password, user.password);
  if (isValid) {
    return { emailAddress: user.email };
  } else {
    return null;
  }
};

module.exports.lookupUser = async email => {
  var user = await getUser(email);
  if (user) {
    return { emailAddress: user.email };
  } else {
    return null;
  }
};

async function getUser(email) {
  const params = {
    TableName: process.env.tableName,
    Key: {
      email: email
    }
  };

  try {
    var result = await dynamoDbLib.call("get", params);
    if (result.Item) {
      return result.Item;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}
