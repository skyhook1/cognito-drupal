# cognito-drupal

Lamda migration function for Drupal passwords

# Steps to setup

0. Install serverless (npm install serverless -g) and aws-cli, and configure them
1. Serverless deploy: serverless deploy --stage dev (or prod when finished testing)
1. Upload the users & passwords to dynamoDB database STAGE-legacyUsers, with primary key as email and password as the only other field.
1. Enable USER_PASSWORD_AUTH in (AWS Console -> User Pool -> App Clients -> Client) and in the react front-end in Amplify.configure add authenticationFlowType: "USER_PASSWORD_AUTH" to Auth.
1. Add function (cognito-drupal-STAGE-migrator) to AWS Console -> User Pool -> Triggers -> User Migration
